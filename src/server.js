import express from "express";

const app = express();

app.use(express.json()); //Middleware to parse request body

app.listen(8080, () => console.log("Server is listening on port 8080"));

//Route Handlers
app.use("/users", (req, res) => {
  res.send("The user is: John Doe");
});

/*
1. Request body (usually json)
2. URL Parameters /products/123
3. Query parameters /products?key=value&key=value
*/

app.get("/products", (req, res) => {
  res.send("The site get request has 100 products");
});

//url parameter and query parameter
//curl http://localhost:8080/product/puma?search=shoes&promo=ABC123
app.get("/product/:productId", (req, res) => {
  //res.send("The site get request with productId" + req.params.productId);
  let { productId } = req.params;
  let { search, promo } = req.query;
  res.send(
    "The site get request with productId " +
      productId +
      ` Also the search string was ${search} and the promocode was ${promo}`
  );
});

//curl -X POST -d '{ "productId": "123" }' -H 'Content-Type: application/json' http://localhost:8080/products
app.post("/products", (req, res) => {
  const body = req.body;
  res.send(`The site post request wit product id is ${body.productId}`);
});

//curl -X POST -d '{ "username": "123", "password": "p123"}' -H 'Content-Type: application/json' http://localhost:8080/products
app.post("/login", (req, res) => {
  let { username, password } = req.body;
  res.send(
    `Logging in user with username ${username} and password ${password}`
  );
});

app.put("/products", (req, res) => {
  res.send("The site put request has 100 products");
});

app.delete("/products", (req, res) => {
  res.send("The site delete request has 100 products");
});

app.use((req, res) => {
  res.send("Unrecognized path");
});
