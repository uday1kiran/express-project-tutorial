## creating a project

```console
npm init -y
npm install express
```

## to run

```console
node src\server.js
```

## using curl

```console
curl http://localhost:8080/products
curl -X POST http://localhost:8080/products
curl -X PUT http://localhost:8080/products
curl -X DELETE http://localhost:8080/products
```

## vscode extension

- for postman alternative - [ Thunder client](https://www.youtube.com/watch?v=Tu5jtyOma4o)
